Python 3.4.1 (v3.4.1:c0e311e010fc, May 18 2014, 10:45:13) [MSC v.1600 64 bit (AMD64)] on win32
Type "copyright", "credits" or "license()" for more information.
>>> import datetime
>>> print(datetime.strptime('10:35', '%H:%M'))
Traceback (most recent call last):
  File "<pyshell#1>", line 1, in <module>
    print(datetime.strptime('10:35', '%H:%M'))
AttributeError: 'module' object has no attribute 'strptime'
>>> print(datetime.datetime.strptime('10:25', '%h:%m'))
Traceback (most recent call last):
  File "<pyshell#2>", line 1, in <module>
    print(datetime.datetime.strptime('10:25', '%h:%m'))
  File "C:\Python34\lib\_strptime.py", line 500, in _strptime_datetime
    tt, fraction = _strptime(data_string, format)
  File "C:\Python34\lib\_strptime.py", line 329, in _strptime
    (bad_directive, format)) from None
ValueError: 'h' is a bad directive in format '%h:%m'
>>> print(datetime.datetime.strptime('10:25', '%H:%M'))
1900-01-01 10:25:00
>>> a = '10:25'
>>> b = '11:25'
>>> a = datetime.datetime.strptime('10:25', '%H:%M')
>>> b = datetime.datetime.strptime('11:25', '%H:%M')
>>> print(a,b)
1900-01-01 10:25:00 1900-01-01 11:25:00
>>> tot = b - a
>>> print(tot)
1:00:00
>>> b = datetime.datetime.strptime('11:47', '%H:%M')
>>> print(tot)
1:00:00
>>> print(a,b)
1900-01-01 10:25:00 1900-01-01 11:47:00
>>> tot= b-a
>>> print(tot)
1:22:00
>>> c = datetime.datetime.strptime('1:00', '%H:%M')
>>> d = datetime.datetime.strptime('1:22', '%H:%M')
>>> t2 = c+d
Traceback (most recent call last):
  File "<pyshell#18>", line 1, in <module>
    t2 = c+d
TypeError: unsupported operand type(s) for +: 'datetime.datetime' and 'datetime.datetime'
>>> th = (tot.seconds) / 3600
>>> print(th)
1.3666666666666667
>>> print(str(th))
1.3666666666666667
>>> def sumtime(*times)
SyntaxError: invalid syntax
>>> def calctime(*times):
	total = 0
	for t in times:
		h,m = t.split(":")
		total += 3600 * int(h) + 60 * int(m)
	return total

>>> print(calctime(*(c,d)))
Traceback (most recent call last):
  File "<pyshell#30>", line 1, in <module>
    print(calctime(*(c,d)))
  File "<pyshell#29>", line 4, in calctime
    h,m = t.split(":")
AttributeError: 'datetime.datetime' object has no attribute 'split'
>>> c = '1.55'
>>> d = '2:15'
>>> print(calctime(*(c,d)))
Traceback (most recent call last):
  File "<pyshell#33>", line 1, in <module>
    print(calctime(*(c,d)))
  File "<pyshell#29>", line 4, in calctime
    h,m = t.split(":")
ValueError: need more than 1 value to unpack
>>> c= "1:55"
>>> d = "2:15"
>>> print(calctime(*(c,d)))
15000
>>> 15000 / 60
250.0
>>> 15000 / 3600
4.166666666666667
>>> e = 15000 / 3600
>>> print(round(e))
4
>>> print("%02d:%02d" % e)
Traceback (most recent call last):
  File "<pyshell#41>", line 1, in <module>
    print("%02d:%02d" % e)
TypeError: not enough arguments for format string
>>> f = 15000 / 60
>>> print("%02d:%02d" % e,f)
Traceback (most recent call last):
  File "<pyshell#43>", line 1, in <module>
    print("%02d:%02d" % e,f)
TypeError: not enough arguments for format string
>>> print("%02d:%02d" % (e,f))
04:250
>>> 
