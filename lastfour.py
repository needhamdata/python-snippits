#!/usr/bin/env python3

import sys

word = sys.argv[1]

pword = word[-4:]

print("The Word You Entered is {word} and the last four characters are {pword}.".format(**locals()))
