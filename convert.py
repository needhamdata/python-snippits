#python script to convert numbers
#Written by: J. Michael Needham
#Original Date Created: Nov 13, 2013
#Modified: Nov 17, 2013 by J. Mike Needham, include credits
#Modified: Nov 17, 2013 by J. Mike Needham, Add some text to clarify
#Version: 0.1a
#
########################################################################

import os

print("**** Convert a Number from HEX ==> DEC or DEC ==> HEX ****\n")
num = input("Enter Number: ")
conv = input("Convert to (h)ex or (d)ec? ")


if conv == "h":
	target = str(hex(int(num)))
	print("DECIMAL: " + str(num) + " is " + target[2:] + " in HEX.\n")
else:
    target = str(int(num,16))
    print("HEX: " + str(num) + " is " + target + " in DECIMAL.\n")

os.system("pause")

    
